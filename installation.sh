#!/usr/bin/env bash

pip3 install flask
pip3 install pyresparser
python3 -m spacy download en_core_web_sm
python3 -m nltk.downloader words